﻿using CGI.BootCamp.Examples.Pagination.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CGI.BootCamp.Examples.Pagination.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index(GridSortOptions options)
        {
            //TODO: - jvd - validate user input
            //TODO: - jvd - default sort column

            var viewModel = new ProductCatalogViewModel()
            {
               GridOptions = options
            };

            using (var connection = new AdventureWorksLT2019Context())
            {
                viewModel.TotalNumberOfResults = connection.Products.Count() ;

                var products = connection.Products.AsQueryable();

                if (!string.IsNullOrEmpty(options.SortColumn))
                {
                    if (options.SortColumn == "productName")
                    {
                        products = products.OrderBy(p => p.Name);
                    }
                    else if(options.SortColumn == "standardCost"){
                        products = products.OrderBy(p => p.StandardCost);
                    }
                }

                viewModel.Records = products
                    .ApplyPaging(options)
                    .ToList();
            }

            return View(viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
