﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CGI.BootCamp.Examples.Pagination.Models
{
    public class GridSortOptions : GridOptions
    {
        public string SortColumn { get; set; }

    }
}
