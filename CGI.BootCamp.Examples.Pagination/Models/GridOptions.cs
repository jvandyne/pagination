﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CGI.BootCamp.Examples.Pagination.Models
{
    public class GridOptions
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public GridOptions()
        {
            this.PageSize = 20;
            this.PageNumber = 1;
        }

      
    }
}
