﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CGI.BootCamp.Examples.Pagination.Models
{
    public class BaseViewModel<TRecord, TGridOptions> where TGridOptions : GridOptions, new()
    {
        public IEnumerable<TRecord> Records { get; set; }
        public int TotalNumberOfResults { get; set; }
        public TGridOptions GridOptions { get; set; }

        public BaseViewModel()
        {
            this.Records = new List<TRecord>().AsQueryable();
            this.GridOptions = new TGridOptions();
        }

        public int GetPageCount()
        {
            var pageCount = (int)Math.Ceiling(Convert.ToDecimal(this.TotalNumberOfResults) / Convert.ToDecimal(this.GridOptions.PageSize));

            return pageCount;
        }
    }
}
