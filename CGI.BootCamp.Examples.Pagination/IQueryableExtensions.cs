﻿using CGI.BootCamp.Examples.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CGI.BootCamp.Examples.Pagination
{
    public static class IQueryableExtensions
    {
        public static IQueryable<TRecord> ApplyPaging<TRecord>(this IQueryable<TRecord> records, GridOptions options){
            return records
                .Skip((options.PageNumber - 1) * options.PageSize)
                .Take(options.PageSize);
        }
    }
}
