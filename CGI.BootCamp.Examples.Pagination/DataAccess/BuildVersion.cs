﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CGI.BootCamp.Examples.Pagination
{
    public partial class BuildVersion
    {
        public byte SystemInformationId { get; set; }
        public string DatabaseVersion { get; set; }
        public DateTime VersionDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
